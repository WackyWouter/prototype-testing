<?php

class database{

    public static $conn = "";
    private static $connection = false;

    public static function connection(){
        if(!defined("DATABASE")){
            die("Error: " . "database unset");
            //TODO make this go to log function
        }
        if(!defined("PASSWORD")){
            die("Error: " . "password unset");
            //TODO make this go to log function
        }
        if(!defined("SERVERNAME")){
            die("Error: " . "servername unset");
            //TODO make this go to log function
        }
        if(!defined("USERNAME")){
            die("Error: " . "username unset");
            //TODO make this go to log function
        }

        self::$conn = new mysqli(SERVERNAME, USERNAME, PASSWORD, DATABASE);

        // Check connection
        if (self::$conn->connect_error) {
            die("Connection failed: " . self::$conn->connect_error);
            //TODO make this go to log function
        }
        self::$connection = true;
        echo "Connected successfully";
    }

    // public static function addCategory($category, $user_id){
    //     if(!isset($category)){
    //         die("Error: " . "category unset");
    //         //TODO make this go to log function
    //     }
    //     if(!isset($user_id)){
    //         die("Error: " . "user_id unset");
    //         //TODO make this go to log function
    //     }

    //     $stmt = self::$conn->prepare("INSERT INTO category(name, user_id) VALUES(?, ?)");
    //     $stmt->bind_param('si', $category, $user_id);
    //     $stmt->execute();
    //     $stmt->close();

    //     // maybe return the added item
    //     return json_encode(['status', 'ok']);
    // }

    public static function addReason($reason){
        if(!self::$connection){
            self::connection();
        }

        $stmt = self::$conn->prepare("INSERT INTO reden(situatie, behoefte, woonsituatie, relatie, educatie, reden) VALUES(?,?,?,?,?,?)");
        $stmt->bind_param('sisiss', $situatie, $behoefte, $woonsituatie, $relatie, $educatie, $reden);
        $stmt->execute();
        $stmt->close;
    }

}
?>