<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="formCont">
            <img src="whispp/logo/Whispp_logo_liggend_kleur01.png" alt="Whispp Logo">
            <form action="backend/security.php" method="get">
                <label id="selectLabel">Is het gebruik prive of zakelijk?</label>
                <select id="select" name="gebruik" required form="selectLabel">
                    <option value="prive">prive</option>
                    <option value="zakelijk">zakelijk</option>
                </select>
    
                <label id="sterkteLab">Hoe sterk is de behoefte?</label>
                <input type="number" min="0" max="10" name="sterkte" required form="sterkteLab" id="sterkte">
    
                <label id="woonLab">Wat is je woon situatie?</label>
                <select id="select" name="woon" required form="woonLab">
                    <option value="alleen">Ik woon alleen</option>
                    <option value="ouders">Ik woon bij mijn ouders</option>
                    <option value="huisgenoten">Ik woon met één of meerdere huisgenoten</option>
                    <option value="partner">Ik woon met mijn partner</option>
                </select>
    
                <label id="relatieLab">Heb je een romantische relatie?</label>
                <select id="select" name="gebruik" required form="selectLabel">
                    <option value="1">ja</option>
                    <option value="0">nee</option>
                </select>

                <label id="eduLab">wat is je opleidings niveau?</label>
                <select id="select" name="gebruik" required form="selectLabel">
                    <option value="basis">Basisschool</option>
                    <option value="vmbo">VMBO</option>
                    <option value="mavo">MAVO</option>
                    <option value="havo">HAVO</option>
                    <option value="vwo">VWO</option>
                    <option value="mbo">MBO</option>
                    <option value="hbo">HBO</option>
                    <option value="uni">universiteit</option>
                </select>
    
                <label id="redenLab">Wat is de reden dat je gebruik wilt maken van Whispp?</label>
                <textarea name="reden" id="reden" required form="redenLab" cols="30" rows="10"></textarea>
    
                <button class="btn" type="submit">submit</button>
            </form>
        </div>
        
   </body>
</html>